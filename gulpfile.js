'use strict';

var gulp = require('gulp');


// Gulp sass
var sass = require('gulp-sass');
sass.compiler = require('sass');

gulp.task('sass', function () {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest('app/css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('app/scss/**/*.scss', gulp.series('sass'));
});


// Gulp useref & JS minify
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');

gulp.task('useref', function () {
    return gulp.src('app/*.html', { allowEmpty: true })
        .pipe(useref())
        // Minifies only if it's a JavaScript file
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulp.dest('dist'))
});


// Gulp images
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

gulp.task('images', function () {
    return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg)')
        // Caching images that ran through imagemin
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('dist/img'))
});


// Gulp fonts
gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
})


// Gulp favicon
gulp.task('favicon', function () {
    return gulp.src('app/favicon/**/*')
        .pipe(gulp.dest('dist/favicon'))
})


// Gulp robots.txt
gulp.task('robots', function () {
    return gulp.src('app/robots.txt')
        .pipe(gulp.dest('dist'))
})


// Gulp delete
var gulp = require('gulp');
var del = require('del');

function clean(cb) {
    del(['dist']);
    cb();
}


// Gulp runsequence
gulp.task('build', gulp.series(
    clean,
    'sass',
    'useref',
    'images',
    'fonts',
    'favicon',
    'robots'
));